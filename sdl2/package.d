module sdl2;

@nogc:
nothrow:

public import sdl2.types;

extern(C)
{
	int SDL_Init(uint flags);
	void SDL_Quit();
	const(char)* SDL_GetError();

	void SDL_Delay(uint ms);

	int SDL_CreateWindowAndRenderer(int w, int h, uint flags, SDL_Window** window, SDL_Renderer** renderer);
	void SDL_DestroyWindow(SDL_Window* window);
	void SDL_DestroyRenderer(SDL_Renderer* window);

	SDL_bool SDL_SetHint(const(char*) name, const(char*) value);
	int SDL_RenderSetLogicalSize(SDL_Renderer* renderer, int w, int h);
	int SDL_RenderClear(SDL_Renderer*);
	int SDL_RenderCopy(SDL_Renderer*, SDL_Texture*, const(SDL_Rect*) src, const(SDL_Rect*) dst);
	void SDL_RenderPresent(SDL_Renderer*);

	SDL_Texture* SDL_CreateTexture(SDL_Renderer* renderer, uint format, int access, int w, int h);
	void SDL_DestroyTexture(SDL_Texture*);
	int SDL_UpdateTexture(SDL_Texture*, const(SDL_Rect*) target, const(void*) pixels, int pitch);
	int SDL_LockTexture(SDL_Texture*, const(SDL_Rect*), void** pixel_data, int* pitch);
	void SDL_UnlockTexture(SDL_Texture*);

	SDL_Surface* SDL_CreateRGBSurface(uint flags, int w, int h, int depth, uint red_mask, uint green_mask, uint blue_mask, uint alpha_mask);
	int SDL_ConvertPixels(int w, int h, uint src_format, const(void*) src, int src_pitch, uint dst_format, void* dst, int dst_pitch);
	pragma(mangle, "SDL_UpperBlit")
	int SDL_BlitSurface(SDL_Surface *src, const(SDL_Rect*) src_rect, SDL_Surface *dst, const(SDL_Rect*) dst_rect);

	int SDL_SetPaletteColors(SDL_Palette*, const(SDL_Color*), int first_color, int num_colors);

	int SDL_WaitEvent(SDL_Event* event);
	int SDL_PushEvent(SDL_Event* event);
	SDL_TimerID SDL_AddTimer(uint interval, SDL_TimerCallback callback, void* param);

	SDL_AudioDeviceID SDL_OpenAudioDevice(const(char*) device, int iscapture, const(SDL_AudioSpec*) desired, SDL_AudioSpec* obtained, int allowed_changes);
	void SDL_PauseAudioDevice(SDL_AudioDeviceID device_id, int on);
	void SDL_CloseAudio();

	int SDL_ShowCursor(int toggle);
}
