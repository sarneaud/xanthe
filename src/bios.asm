; From
; https://gitlab.com/sarneaud/hello-d-x86-bios
; 
; Modified to include a switch to VGA graphics mode 0x13.  Under same MIT licence as original.

section boot exec align=16
bits 16

extern systemEntry
extern stack_address
extern bss_start
extern bss_size
extern payload_start
extern num_payload_sectors
extern disk_buffer
extern disk_buffer_real_mode_segment

; Preprocessor Definitions

%define sectors_per_read 126

%include "common_defs.asm"

; Disk Access Packet Structure Definition
struc dapa
	dapa_structure_size: resb 1
	dapa_zero: resb 1
	dapa_num_sectors: resb 2
	dapa_dst_offset: resb 2
	dapa_dst_segment: resb 2
	dapa_lba_lo: resb 4
	dapa_lba_hi: resb 4
endstruc

; Bootloader Code

start:
	mov [boot_disk], dl
	cli
	xor ax, ax
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov sp, 0x7000
	sti

	mov ax, 0x0013
	int 0x10

	; Activate A20
	mov ax, 0x2401
	int 0x15

	.load_loop:
	call load_payload_or_execute
	cmp dword [payload_sectors_remaining], 0
	jnz .load_loop

load_payload_or_execute:
	; Figure out how many sectors to load
	mov word [disk_address_packet+dapa_num_sectors], sectors_per_read
	cmp dword [payload_sectors_remaining], sectors_per_read
	ja .full_read
	mov ax, [payload_sectors_remaining]
	mov [disk_address_packet+dapa_num_sectors], ax
	.full_read:

	; Read data
	mov ah, 0x42
	mov si, disk_address_packet
	mov dl, [boot_disk]
	mov cx, 10  ; Up to 10-1=9 attempts
	.retry:
	dec cx
	jz shutdown
	int 0x13
	jc .retry

	; Keep track of how much we've read so far and where to put the next chunk of data
	xor eax, eax
	mov ax, [disk_address_packet+dapa_num_sectors]
	sub [payload_sectors_remaining], eax
	add [disk_address_packet+dapa_lba_lo], eax
	adc dword [disk_address_packet+dapa_lba_hi], 0
	shl ax, 7 ; Number of sectors -> number of 4B dwords
	mov [last_read_dwords], eax

	cli
	mov [real_mode_sp], sp

	lgdt [gdtr]
	; Enable protected mode
	mov eax, cr0
	or al, 1
	mov cr0, eax

	jmp seg_code32:.enter_32b
	.enter_32b: bits 32
	mov ax, seg_data
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	mov esp, stack_address

	mov eax, disk_buffer
	mov esi, eax
	mov edi, [payload_write_ptr]
	mov ecx, [last_read_dwords]
	rep movsd
	mov [payload_write_ptr], edi

	cmp dword [payload_sectors_remaining], 0
	jnz .skip_payload_execute

	push dword 0x20
	call move_irqs
	add sp, 4
	; Mask all IRQs for now
	mov al, 0xff
	out 0x21, al
	out 0xa1, al

	mov edi, bss_start
	mov ecx, bss_size
	xor al, al
	rep stosb

	finit

	call systemEntry

	cli
	push dword 0x0
	call move_irqs
	add sp, 4
	.skip_payload_execute:

	; Break back into 16b real mode
	cli
	jmp seg_code16:.enter_16b
	.enter_16b: bits 16
	mov eax, cr0
	and al, 0xfe
	mov cr0, eax
	jmp 0:.real_mode
	.real_mode:
	xor ax, ax
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov esp, [real_mode_sp]
	lidt [real_mode_idtr]
	sti

	ret

%include "common.asm"

; Variables

boot_disk db 0

real_mode_sp: dd 0

align 4
disk_address_packet:
	istruc dapa
		at dapa_structure_size, db 16
		at dapa_zero, db 0
		at dapa_num_sectors, dw sectors_per_read
		at dapa_dst_offset, dw 0
		at dapa_dst_segment, dw disk_buffer_real_mode_segment
		at dapa_lba_lo, dd 1
		at dapa_lba_hi, dd 0
	iend

payload_write_ptr dd payload_start
payload_sectors_remaining dd num_payload_sectors
last_read_dwords dw 0

times 510-($-$$) db 0
dw 0xaa55
