bits 32
move_irqs:
	push ebp
	mov ebp, esp

	mov al, 0x11
	out 0x20, al
	out 0xa0, al

	mov al, [ebp+8]
	out 0x21, al
	add al, 0x08
	out 0xa1, al

	mov al, 0x04
	out 0x21, al
	mov al, 0x02
	out 0xa1, al

	mov al, 0x01
	out 0x21, al
	out 0xa1, al

	mov esp, ebp
	pop ebp
	ret

bits 16
shutdown:
	; APM shutdown
	mov ax, 0x5300
	xor bx, bx
	int 0x15
	jc shutdown_fail

	mov ax, 0x5303
	xor bx, bx
	int 0x15
	jc shutdown_fail

	mov ax, 0x5308
	mov bx, 0x0001
	mov cx, 0x0001
	int 0x15
	jc shutdown_fail

	mov ax, 0x5307
	mov bx, 0x0001
	mov cx, 0x0003
	int 0x15

	shutdown_fail:
	cli
	hlt
	jmp shutdown_fail

; For accessing real mode (BIOS) interrupts
real_mode_idtr:
	dw 0x3ff
	dd 0x0

; Global Descriptor Table
align 4
dw 0
gdtr:
	dw 4*dt_entry_size-1
	dd gdt
gdt:
	times dt_entry_size db 0 ; null entry
	istruc dt_entry ; code32
		at dt_limit_lo, dw 0xffff
		at dt_base_lo, db 0, 0, 0
		at dt_access, db 0b10011010
		at dt_limit_hi, db 0b11001111
		at dt_base_hi, db 0
	iend
	istruc dt_entry ; data
		at dt_limit_lo, dw 0xffff
		at dt_base_lo, db 0, 0, 0
		at dt_access, db 0b10010010
		at dt_limit_hi, db 0b11001111
		at dt_base_hi, db 0
	iend
	istruc dt_entry ; code16
		at dt_limit_lo, dw 0xffff
		at dt_base_lo, db 0, 0, 0
		at dt_access, db 0b10011010
		at dt_limit_hi, db 0b10001111
		at dt_base_hi, db 0
	iend

