module os.system;
@nogc:
nothrow:

import core.atomic : atomicLoad, atomicOp, atomicStore;
public import os.drivers;

version (sdl)
{
	version = threaded;
}

version (bios)
{
	version = interrupt_driven;
	version = nolibc;
}

version (multiboot)
{
	version = interrupt_driven;
	version = nolibc;
}

alias Tick = uint;

struct ColorSpec
{
	ubyte r, g, b;
}

// VGA mode 0x13 properties
enum kScreenWidth = 320;
enum kScreenHeight = 200;

enum KeyCode
{
	Other = 0,
	Space,
	Left,
	Right,
	Escape,
}
// NB: isKeyDown(KeyCode.Other) has no reliable meaning in normal use, but KeyCode.Other exists so that "press any key"
// functionality works reasonably well.

bool isKeyDown(KeyCode k)
{
	return atomicLoad(key_table[k]);
}

void waitForKeyPress()
{
	bool any_key_down;
	do
	{
		import os.sound : updateSound;
		updateSound();
		waitForEvent();
		foreach (is_key_down; key_table[])
		{
			any_key_down |= is_key_down;
		}
	} while (!any_key_down);
}

Tick getTick()
{
	return atomicLoad(tick);
}

void waitForTick(Tick until)
{
	while (getTick() < until)
	{
		import os.sound : updateSound;
		updateSound();
		waitForEvent();
	}
}

void delayTicks(Tick num_ticks)
{
	waitForTick(getTick() + num_ticks);
}

uint getRandomSeed()
{
	// The x86 keeps a 64b count of the number of processor cycles since boot, called the Time Stamp Counter (TSC).
	// It's read using the rdtsc instruction, and it's a cheap way to get a random seed that's good enough for a game.
	uint ret;
	asm @nogc nothrow
	{
		rdtsc;
		xor EAX, EDX;
		// This just a randomly generated number that has a roughly even number of set bits and is relatively prime to 2**32.
		// Multiplying a 32b number by such a value is a pseudo-random linear transformation.
		// It just means that even if we getRandomSeed() at close timestamps, the result will be quite different.
		mov EDX, 2637321879;
		mul EAX, EDX;
		mov ret, EAX;
	}
	return ret;
}

version(nolibc)
{
	extern(C) void* memcpy(void* dest, const(void)* src, size_t n)
	{
		// This isn't optimal, but life is too short for reimplementing efficient memcpy for a simple demo game.
		auto dest_8 = cast(ubyte*) dest;
		auto src_8 = cast(const(ubyte)*) src;
		while (n--)
		{
			*dest_8++ = *src_8++;
		}
		return dest;
	}

	extern(C) int memcmp(const(void)* s1, const(void)* s2, size_t n)
	{
		auto p1 = cast(const(ubyte)*)s1;
		auto p2 = cast(const(ubyte)*)s2;
		while (n--)
		{
			auto diff = cast(int)*p1 - cast(int)*p2;
			if (diff != 0) return diff;
			++p1;
			++p2;
		}
		return 0;
	}
}

package:

void setKeyState(KeyCode key, bool is_down)
{
	atomicStore(key_table[key], is_down);
}

void incrementTick()
{
	atomicOp!"+="(tick, 1);
}

private:

shared bool[KeyCode.max+1] key_table;

shared Tick tick;
