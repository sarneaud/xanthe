module os.sound;
@nogc:
nothrow:

/* Audio gets buffered in the main process by regularly calling updateSound() because not all backends have the luxury of threading.
   The buffer empty condition is signalled from a dedicated audio thread in SDL, and through an interrupt on bare-metal x86.
   This makes synchronisation a little trickier because there's no one-size-fits-all solution.  The threaded backends can use a mutex,
   but this would deadlock on a single-threaded, interrupt-driven backend.  Instead of locking, interrupts are disabled when needed
   in bare-metal modes.
*/

import os.system : initSoundBackend, getTick, Tick;
version (threaded)
{
	import core.sync.mutex;
}

void initSound(immutable(Sound) background_music)
{
	setMusic(background_music);
	initSoundBackend(44100, kNumSamplesPerBuffer);
	output_gain = 0.0;
	last_gain_update_tick = getTick();

	version (threaded)
	{
		lock = new Mutex();
	}
}

void updateSound()
{
	{
		enterCritical();
		scope(exit) leaveCritical();

		if (output_buffer_empty)
		{
			fillOutputBuffer();
		}
	}

	auto cur_tick = getTick();
	while (last_gain_update_tick < cur_tick)
	{
		output_gain *= output_gain_multiplier;
		if (output_gain >= 1.0)
		{
			output_gain = 1.0;
			output_gain_multiplier = 1.0;
		}
		last_gain_update_tick++;
	}
}

void setMusic(immutable(Sound) background_music)
{
	auto music_data_ptr = cast(immutable(short)*) background_music._sound_data.ptr;
	music = Music(music_data_ptr, music_data_ptr, music_data_ptr + background_music._sound_data.length / 2);

	output_gain_multiplier = 1.1;
}

void fadeInSound(double multiplier)
{
	assert (multiplier > 1.0);
	if (output_gain == 0.0)
	{
		output_gain = 0.05;
	}
	output_gain_multiplier = multiplier;
}

void fadeOutSound(double multiplier)
{
	assert (multiplier < 1.0);
	output_gain_multiplier = multiplier;
}

void outputBufferEmptyCallback(short* new_output_buffer, uint new_num_samples)
{
	version (threaded)
	{
		lock.lock();
		scope(exit) lock.unlock();
	}

	output_buffer_empty = true;
	output_buffer = new_output_buffer;
	num_output_buffer_samples = new_num_samples;
}

struct Sound
{
	@nogc:
	nothrow:

	static immutable(Sound) load(string filename, bool is_music = false)()
	{
		auto data = cast(immutable(ubyte[])) import(filename);
		if (!is_music)
		{
			assert (data.length % 8 == 0);  // Needs to be 16b audio that can be split into blocks of 4 samples
		}
		return Sound(data[]);
	}

	void play() const
	{
		foreach (ref voice; voices)
		{
			if (voice.sound_data.length == 0)
			{
				voice.sound_data = (cast(immutable(short)*)_sound_data.ptr)[0 .. _sound_data.length / 2];
				return;
			}
		}
		// Just give up if there aren't any spare voices
		debug
		{
			import core.stdc.stdio;
			fputs("Warning: out of voices\n", stderr);
		}
	}

	private:
	immutable(ubyte)[] _sound_data;
}

private:

enum kNumVoices = 16;
enum kNumSamplesPerBuffer = 2048;

__gshared
{
	Voice[kNumVoices] voices;
	Music music;
	double output_gain = 0.0, output_gain_multiplier = 1.0;
	Tick last_gain_update_tick;

	bool output_buffer_empty = false;
	short* output_buffer;
	uint num_output_buffer_samples;

	version (threaded)
	{
		Mutex* lock;
	}
}

void fillOutputBuffer()
{
	music.bufferData(output_buffer, num_output_buffer_samples);

	foreach (ref voice; voices)
	{
		if (voice.sound_data.length > 0)
		{
			import std.algorithm : min;
			auto len = min(num_output_buffer_samples, voice.sound_data.length);
			auto buffer = voice.sound_data[0..len];
			voice.sound_data = voice.sound_data[len .. $];
			mixSound(output_buffer, buffer.ptr, len);
		}
	}

	auto gain = output_gain;
	foreach (j; 0 .. num_output_buffer_samples)
	{
		output_buffer[j] = cast(short) (output_buffer[j] * gain);
	}

	num_output_buffer_samples = 0;
	output_buffer_empty = false;
	debug
	{
		output_buffer = null;
	}
}


struct Voice
{
	immutable(short)[] sound_data;
}

struct Music
{
	@nogc:
	nothrow:

	void bufferData(short* output_buffer, uint num_samples)
	{
		while (num_samples > 0)
		{
			auto samples_remaining_in_cycle = _end - _cur;

			if (num_samples < samples_remaining_in_cycle)
			{
				copy(output_buffer, num_samples);
				num_samples = 0;
			}
			else
			{
				copy(output_buffer, samples_remaining_in_cycle);
				output_buffer += samples_remaining_in_cycle;
				num_samples -= samples_remaining_in_cycle;
				assert (_cur == _end);
				_cur = _start;
			}
		}
	}

	void copy(short* output_buffer, size_t num_samples) @nogc nothrow
	{
		while (num_samples--)
		{
			*output_buffer++ = *_cur++;
		}
	}

	immutable(short)* _cur, _start, _end;
}

void mixSound(short* dst, const(short*) src, uint num_samples)
{
	// Using MMX here isn't so much for speed but because it's designed exactly for this kind of application, so this kind
	// of implementation would be expected in an embedded system.
	// MMX natively supports saturating arithmetic.

	assert (num_samples % 4 == 0);
	uint n = num_samples / 4;

	// Need to use RDI/RSI in 64b builds and EDI/ESI in 32b builds
	// This would be dead simple using the C preprocessor.  It's hard to do with D mixins because importing std.format
	// pulls in lots of runtime depenedencies, even if std.format is only used in CTFE.  Leaving this here as an example
	// of something that could be improved.  (Suggestions welcome.)

	version (X86)
	{
		auto dst_addr = cast(uint)dst, src_addr = cast(uint)src;
		asm @nogc nothrow
		{
			mov EDI, dst_addr;
			mov ESI, src_addr;
		}
	}
	version (X86_64)
	{
		auto dst_addr = cast(ulong)dst, src_addr = cast(ulong)src;
		asm @nogc nothrow
		{
			mov RDI, dst_addr;
			mov RSI, src_addr;
		}
	}

	asm @nogc nothrow
	{
		mov ECX, n;
loop:
		movq MM0, [EDI];
		paddsw MM0, [ESI];
		movq [EDI], MM0;
	}

	version (X86)
	{
		asm @nogc nothrow
		{
			add EDI, 8;
			add ESI, 8;
		}
	}
	version (X86_64)
	{
		asm @nogc nothrow
		{
			add RDI, 8;
			add RSI, 8;
		}
	}

	asm @nogc nothrow
	{
		dec ECX;
		jnz loop;

		emms;
	}
}

void enterCritical()
{
	version (interrupt_driven)
	{
		asm @nogc nothrow
		{
			cli;
		}
	}
	version (threaded)
	{
		lock.lock();
	}
}

void leaveCritical()
{
	version (interrupt_driven)
	{
		asm @nogc nothrow
		{
			sti;
		}
	}
	version (threaded)
	{
		lock.unlock();
	}
}
