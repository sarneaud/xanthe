module os.graphics;
@nogc:
nothrow:

import os.geometry;
import os.system;

enum kScreenBufferSize = kScreenWidth * kScreenHeight;

struct Screen
{
	@nogc:
	nothrow:

	debug
	{
		static immutable uint kCanaryValue = 0xdeadbeef;

		@property uint* canary1() pure
		{
			return (cast(uint*)_buffer) - 1;
		}

		@property uint* canary2() pure
		{
			return cast(uint*)(_buffer + kScreenBufferSize);
		}

		void checkIntegrity() pure
		{
			assert (*canary1 == kCanaryValue);
			assert (*canary2 == kCanaryValue);
		}

		void setCanaries() pure
		{
			*canary1 = *canary2 = kCanaryValue;
		}
	}

	private:
	ubyte* _buffer;
}

Screen getScreen()
{
	return Screen(getScreenBuffer());
}

debug
{
	// Extra bytes each side for detecting overflow in tests
	ubyte[kScreenBufferSize + 2 * Screen.kCanaryValue.sizeof] test_screen;
	Screen getTestScreen()
	{
		test_screen = 0;
		auto ret = Screen(test_screen.ptr + Screen.kCanaryValue.sizeof);
		ret.setCanaries();
		return ret;
	}
}

struct PcxImage
{
	@nogc:
	nothrow:

	static immutable(PcxImage) load(string filename)()
	{
		auto data = cast(immutable(ubyte[])) import(filename);
		auto ret = immutable(PcxImage)(data);
		assert (ret.data[0] == 10); // Must be PCX
		assert (ret.data[2] == 1); // with run length encoding
		assert (ret.data[3] == 8); // and 8-bit (256 colour) mode
		return ret;
	}

	@property
	uint width() const pure
	{
		return header.x_max - header.x_min + 1;
	}

	@property
	uint height() const pure
	{
		return header.y_max - header.y_min + 1;
	}

	private:

	@property
	auto header() inout pure
	{
		return cast(inout PcxHeader*) data.ptr;
	}

	@property
	auto pixel_data () inout pure
	{
		return data[PcxHeader.sizeof .. $];
	}

	immutable(ubyte[]) data;
}

struct Palette
{
	// Same .col palette format as used by Autodesk Animator Pro
	struct Header
	{
		align(1):
		uint size;
		ushort magic;
		ushort col_version;
	}
	struct Format
	{
		Header header;
		ColorSpec[256] palette;
	}

	void activate() const @nogc nothrow
	{
		auto col_file = cast(immutable(Format)*) data.ptr;
		setPalette(col_file.palette);
	}

	static immutable(Palette) load(string filename)() pure
	{
		auto data = cast(immutable(ubyte[])) import(filename);
		auto ret = immutable(Palette)(data);
		// A full palette in this format will always have this header
		assert (ret.data[0 .. 8] == [0x08, 0x03, 0x00, 0x00, 0x23, 0xb1, 0x00, 0x00]);
		return ret;
	}

	private:

	immutable(ubyte[]) data;
}

void render(const PcxImage image, Screen screen, Vector pos) pure
{
	render(image, screen, screenX(pos), screenY(pos));
}

void render(const PcxImage image, Screen screen, int x, int y) pure
{
	import std.algorithm : max, min;
	immutable int width = image.width;
	immutable int height = image.height;
	immutable int y_limit = min(kScreenHeight, y + height);
	immutable int x_limit = min(kScreenWidth, x + width);

	if (y >= kScreenHeight || y_limit < 0 || x >= kScreenWidth || x_limit < 0) return;

	// Aseprite spoils us and makes nice PCX files where this is true and life is simple.
	// If this fails then either
	// a) decodePcxLine() needs to be adapted to take scan lines into account
	// b) the PCX file needs to be recoded with a nicer encoder :)
	assert (width == image.header.bytes_per_plane_line);

	ubyte[kScreenWidth] buffer;
	assert (width <= kScreenWidth);

	auto data_ptr = image.pixel_data.ptr;

	while (y < 0)
	{
		data_ptr = decodePcxLine(data_ptr, width, buffer.ptr);
		++y;
	}

	auto line_start_ptr = screen._buffer + y * kScreenWidth + x;
	auto end_line = line_start_ptr + (y_limit - y) * kScreenWidth;
	// No X clipping is needed in the common case, and we can render directly to the screen.
	if (x >= 0 && x + width <= kScreenWidth)
	{
		while (line_start_ptr != end_line)
		{
			debug
			{
				assert (y < y_limit);
				y++;
			}
			data_ptr = decodePcxLine(data_ptr, width, line_start_ptr);
			line_start_ptr += kScreenWidth;
		}
	}
	else
	{
		// To clip, the whole line will be copied into a buffer, then the first x_offset bytes will be ignored, then
		// the next copy_length bytes after that will be rendered, and then the rest discarded
		immutable int x_offset = max(0, -x);
		immutable int copy_length = x_limit - x - x_offset;
		assert (x + x_offset + copy_length <= kScreenWidth);
		line_start_ptr += x_offset;
		end_line += x_offset;
		while (line_start_ptr != end_line)
		{
			debug
			{
				assert (y++ < y_limit);
			}
			buffer = 0;
			data_ptr = decodePcxLine(data_ptr, width, buffer.ptr);
			auto buffer_src = buffer.ptr + x_offset;
			auto screen_dst = line_start_ptr;
			foreach (j; 0 .. copy_length)
			{
				assert (buffer_src < &buffer[$-1] + 1);
				assert (screen._buffer <= screen_dst && screen_dst < screen._buffer + kScreenBufferSize);
				immutable c = *buffer_src++;
				if (c == 0)
				{
					++screen_dst;
				}
				else
				{
					*screen_dst++ = c;
				}
			}
			line_start_ptr += kScreenWidth;
		}
	}
	debug
	{
		assert (y == y_limit);
	}
}

void putPixel(Screen screen, Vector pos, ubyte c) pure
{
	putPixel(screen, screenX(pos), screenY(pos), c);
}

void putPixel(Screen screen, uint x, uint y, ubyte c) pure
{
	assert (x < kScreenWidth);
	assert (y < kScreenHeight);
	screen._buffer[y * kScreenWidth + x] = c;
}

uint screenX(Vector v) pure
{
	return cast(uint) (v.x * kScreenWidth / kWorldWidth);
}

uint screenY(Vector v) pure
{
	return cast(uint) (v.y * kScreenHeight / kWorldHeight);
}

private:

immutable(ubyte)* decodePcxLine(immutable(ubyte)* pixel_data, int width, ubyte* output) pure
{
	// PCX uses a simple run-length encoding scheme

	auto line_end = output + width;

	while(output != line_end)
	{
		assert (output < line_end);  // This can happen if using non-standards-compliant PCX encoder that can wrap runs around scan lines.
		if (*pixel_data >= 0xc0)
		{
			auto length = *pixel_data++ - 0xc0;
			immutable c = *pixel_data++;
			if (c == 0)
			{
				// Transparency
				output += length;
			}
			else
			{
				while (length--)
				{
					*output++ = c;
				}
			}
		}
		else
		{
			immutable c = *pixel_data++;
			if (c == 0)
			{
				// Transparency
				++output;
			}
			else
			{
				*output++ = c;
			}
		}
	}

	return pixel_data;
}

debug
{
	immutable test_images = [
		PcxImage.load!"test/1.pcx",
		PcxImage.load!"test/2.pcx",
	];
	immutable test_images_decoded = [
		cast(immutable(ubyte[])) import ("test/1.bin"),
		cast(immutable(ubyte[])) import ("test/2.bin"),
	];
}

unittest
{
	static void test(PcxImage image, uint width, uint height, immutable(ubyte[]) golden_data) @nogc nothrow
	{
		immutable ubyte background = 17;
		assert (image.width == width);
		assert (image.height == height);
		assert (golden_data.length == width * height);

		ubyte[64] buffer;
		assert (image.width <= buffer.length);  // Increase if needed

		auto data_ptr = image.pixel_data.ptr;

		foreach (y; 0 .. image.height)
		{
			buffer = background;
			data_ptr = decodePcxLine(data_ptr, width, buffer.ptr);
			auto offset = y * width;
			auto golden = golden_data[offset .. offset + width];

			foreach (x; 0 .. width)
			{
				if (golden[x] == 0)
				{
					assert (buffer[x] == background);
				}
				else
				{
					assert (buffer[x] == golden[x]);
				}
			}
		}
	}

	test(test_images[0], 32, 32, test_images_decoded[0]);
	test(test_images[1], 5, 3, test_images_decoded[1]);
}

unittest
{
	static void test(PcxImage image, immutable(ubyte[]) golden_data) @nogc nothrow
	{
		import std.algorithm : max;
		int delta = max(image.width, image.height) / 2;
		Vector[7] positions = [
			Vector(0, 0),
			Vector(-delta, 0),
			Vector(0, -delta),
			Vector(-delta, -delta),
			Vector(kScreenWidth - delta, delta),
			Vector(delta, kScreenHeight - delta),
			Vector(kScreenWidth - delta, kScreenHeight - delta),
		];

		immutable ubyte background_val = 17;
		auto screen = getTestScreen();

		foreach (pos; positions)
		{
			screen._buffer[0 .. kScreenBufferSize] = background_val;
			image.render(screen, pos);
			screen.checkIntegrity();

			foreach (x; 0 .. image.width)
			{
				foreach (y; 0 .. image.height)
				{
					int screen_x = x + screenX(pos);
					int screen_y = y + screenY(pos);
					if (screen_x < 0 || screen_x >= kScreenWidth || screen_y < 0 || screen_y >= kScreenHeight) continue;
					auto screen_ptr = screen._buffer + screen_y * kScreenWidth + screen_x;
					auto golden = golden_data[y * image.width + x];
					if (golden == 0)
					{
						assert (*screen_ptr == background_val);
					}
					else
					{
						assert (*screen_ptr == golden);
					}
				}
			}
		}
	}

	foreach (j; 0 .. test_images.length)
	{
		auto screen = getTestScreen();
		test(test_images[j], test_images_decoded[j]);
	}
}

struct PcxHeader
{
	align(1):
	ubyte magic, pcx_version, encoding, bits_per_plane;
	ushort x_min, y_min, x_max, y_max, v_dpi, h_dpi;
	ubyte[48] palette;
	ubyte reserved, num_planes;
	ushort bytes_per_plane_line, palette_type, h_screen_size, v_screen_size;
	ubyte[54] padding;
}
