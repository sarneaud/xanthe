module game.defs;

import std.algorithm : max;
import std.meta : Alias, AliasSeq, allSatisfy, NoDuplicates, staticMap;

import os.geometry;
import os.utils : FreeList;

import game.effects : FarStar, NearStar, Spark;
import game.enemies : Cannon, CannonBolt, Meteoroid, Mine, Orb, Truss;
import game.player : Missile, Player;
import game.rigid_body : RigidBody;

alias RigidBodyTypes = AliasSeq!(Cannon, CannonBolt, FarStar, Meteoroid, Mine, Missile, NearStar, Orb, Player, Spark, Truss);

// TODO: these utilities should be in Phobos
template staticFold(alias F, alias Seed, T...)
{
    static if (T.length == 0)
    {
        alias staticFold = Seed;
    }
    else
    {
        alias staticFold = F!(T[0], staticFold!(F, Seed, T[1..$]));
    }
}

template AliasLift(alias f)
{
    alias AliasLift(Args...) = Alias!(f(Args));
}

alias staticSum(Args...) = staticFold!(AliasLift!( (a,b) => a + b), Alias!0, Args);

enum Sizeof(T) = Alias!T.sizeof;
enum kStorageBufferSize = max(staticMap!(Sizeof, RigidBodyTypes));

enum MaxNumInstances(T) = Alias!T.kMaxNumInstances;
enum kMaxNumBodies = staticSum!(staticMap!(MaxNumInstances, RigidBodyTypes));
enum kMaxNumAllocations = kMaxNumBodies;
enum kMaxZIndex = 5;

alias Allocator = FreeList!kStorageBufferSize;

private:
enum TypeId(T) = Alias!T.kTypeId;
enum TypeIds = staticMap!(TypeId, RigidBodyTypes);
static assert (NoDuplicates!TypeIds == TypeIds);
enum ZIndexOkay(T) = T.kZIndex <= kMaxZIndex;
static assert (allSatisfy!(ZIndexOkay, RigidBodyTypes));
