module game.player;
@nogc:
nothrow:

import game.environment;
import game.effects : sparkShower;
import game.rigid_body;

import os.graphics;
import os.sound : Sound;
import os.system : isKeyDown, KeyCode, Tick;

struct Player
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Player;

	void renderHealth(Screen screen) pure
	{  
		// Using a local, constantly seeded PRNG gives us a random texture that doesn't change between frames.
		import os.utils : XorShiftRng;
		auto rng = XorShiftRng(2);

		enum kMidValue = 24;
		enum kShadeStrength = 4;
		enum kBarX = 10;
		enum kBarY = 10;

		enum kHealthBarScale = 3;
		immutable full_width = kInitialHealth * kHealthBarScale;
		auto display_width = _display_health * kHealthBarScale;
		auto true_width = health * kHealthBarScale;

		immutable(uint[3]) shade = [kShadeStrength, 0, -kShadeStrength];

		uint[3] prev_column = [kMidValue + 2, kMidValue, kMidValue - 1];

		ubyte hue = 80; // Blue

		foreach (x; 0 .. full_width)
		{
			if (x >= true_width)
			{
				hue = 112; // Red
			}
			if (x >= display_width)
			{
				hue = 16; // Grey
			}
			uint[3] new_column;
			foreach (y; 0 .. new_column.length)
			{
				new_column[y] = kMidValue + rng.getRandom(7) - 3;
				new_column[y] = (prev_column[y] + 2 * new_column[y]) / 3;
			}

			uint[3] smoothed = [
				(2*new_column[0] + new_column[1]) / 3,
				(new_column[0] + 2 * new_column[1] + new_column[2]) / 4,
				(new_column[1] + 2 * new_column[2]) / 3,
			];
			
			debug
			{
				foreach (c; smoothed[])
				{
					assert (c >= kShadeStrength && c < 32 - kShadeStrength);
				}
			}

			foreach (ubyte y; 0 .. smoothed.length)
			{
				screen.putPixel(kBarX + x, kBarY + y, cast(ubyte)(hue + smoothed[y] + shade[y]));
			}
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Player player;
		player.renderHealth(screen);
	}

	struct VFuncs
	{
		@nogc:
		nothrow:

		static bool doAction(Environment* env, RigidBody* mass)
		{
			auto player = cast(Player*) mass;
			assert (player == env.player);

			if (player._display_health > player.health)
			{
				player._display_health--;
			}
			if (player._display_health < player.health)
			{
				player._display_health++;
			}

			final switch(player.state)
			{
				case RigidBody.State.Active:
					if (isKeyDown(KeyCode.Space) && env.tick >= player._reload_complete_tick)
					{
						auto missile_mid_pos = player.pos + Vector(Player.kDim.x / 2, -Missile.kDim.y);
						Vector offset1, offset2;
						if (player.v.x < -kBankVelocity || player.v.x > kBankVelocity)
						{
							offset1 = Vector(-5, 0);
							offset2 = Vector(4, 0);
						}
						else
						{
							offset1 = Vector(-4, 0);
							offset2 = Vector(3, 0);
						}
						auto missile1_pos = missile_mid_pos + offset1;
						auto missile2_pos = missile_mid_pos + offset2;
						auto missile1 = Missile.make(env, missile1_pos);
						Missile.make(env, missile2_pos);
						if (missile1 !is null)
						{
							_fire_sound.play();
							player._reload_complete_tick = env.tick + kReloadDelay;
						}
					}

					auto new_a = Vector(0, 0);
					if (isKeyDown(KeyCode.Left))
					{
						new_a -= kRocketAcceleration;
					}
					if (isKeyDown(KeyCode.Right))
					{
						new_a += kRocketAcceleration;
					}
					player.a = new_a;
					break;

				case RigidBody.State.Exploding:
					sparkShower(env, player.pos + player.kDim/2, 4, 16.0);
					break;

				case RigidBody.State.GameOver:
					if (env.game_state == Environment.GameState.Won)
					{
						player.a = Vector(0, -0.4);
					}
					break;
			}

			if (player.pos.x < 0)
			{
				player.pos.x = 0;
				player.v /= 2;
			}
			if (player.pos.x > kWorldWidth - 1 - Player.kDim.x)
			{
				player.pos.x = kWorldWidth - 1 - Player.kDim.x;
				player.v /= 2;
			}

			if (isKeyDown(KeyCode.Escape))
			{
				player.takeDamage(env, kInitialHealth, kTypeId);
			}

			return true;
		}

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			if (mass.health > 0)
			{
				if (mass.v.x < -kBankVelocity )
				{
					_sprite_left.render(screen, mass.pos);
				}
				else if (mass.v.x > kBankVelocity)
				{
					_sprite_right.render(screen, mass.pos);
				}
				else
				{
					_sprite_normal.render(screen, mass.pos);
				}
			}
			else
			{
				_sprite_broken.render(screen, mass.pos);
			}
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Player player;
		player.render(screen, 0);

		player.v = Vector(-20, 0);
		player.render(screen, 0);

		player.v = Vector(20, 0);
		player.render(screen, 0);

		player.state = RigidBody.State.Exploding;
		player.render(screen, 0);
	}

	enum kMaxNumInstances = 1;
	enum kZIndex = 3;
	enum kInitialV = Vector(0, 0);
	enum kInitialA = Vector(0, 0);
	enum kInitialHealth = 60;
	enum kDim = Vector(10, 10);
	enum kDrag = 0.1;
	enum kTeam = RigidBody.Team.Player;
	enum kCollisionDamage = 100;
	enum kExplosionSpeed = 10.0;
	enum kExplosionEnergy = 64;
	enum kPersistAfterExplosion = true;
	static immutable Sound[] explosion_sounds = [
		Sound.load!"player_explosion.raw",
	];

	private:

	enum kBankVelocity = 1;
	enum kReloadDelay = 2;
	enum kRocketAcceleration = Vector(1, 0);

	static immutable _sprite_normal = PcxImage.load!"player/player.pcx";
	static immutable _sprite_left = PcxImage.load!"player/player_left.pcx";
	static immutable _sprite_right = PcxImage.load!"player/player_right.pcx";
	static immutable _sprite_broken = PcxImage.load!"player/player_broken.pcx";

	static immutable _fire_sound = Sound.load!"player_fire.raw";

	Tick _reload_complete_tick;
	int _display_health;
}

unittest
{
	Player p;
	assert (p.type_label == "Player");
	assert (p.type_id == Player.kTypeId);
}

struct Missile
{
	@nogc:
	nothrow:

	mixin DeriveRigidBody!Missile;

	struct VFuncs
	{
		@nogc:
		nothrow:

		static void render(Screen screen, RigidBody* mass, Tick tick) pure
		{
			_sprite.render(screen, mass.pos);
		}

		static bool doAction(Environment* env, RigidBody* mass)
		{
			return mass.pos.y >= 0;
		}
	}

	unittest
	{
		auto screen = getTestScreen();
		Missile missile;
		missile.render(screen, 0);
	}

	enum kMaxNumInstances = 34;
	enum kZIndex = 4;
	enum kInitialV = Vector(0, 0);
	enum kInitialA = Vector(0, -2);
	enum kInitialHealth = 1;
	enum kDim = Vector(1, 5);
	enum kDrag = 0.3;
	enum kTeam = RigidBody.Team.Player;
	enum kCollisionDamage = 1;
	enum kExplosionSpeed = 4.0;
	enum kExplosionEnergy = 1;
	static immutable Sound[] explosion_sounds = [];

	private:

	static immutable _sprite = PcxImage.load!"missile.pcx";
}
