DMD=dmd
DFLAGS=-wi -g -Jgraphics -Jsound -Isrc
BETTERC_DFLAGS:=-c -boundscheck=off -release -betterC -debuglib= -defaultlib= -O -m32 ${DFLAGS}
QEMU=qemu-system-i386
QEMU_FLAGS=-cpu host -enable-kvm -soundhw sb16 -serial stdio

GENERIC_SRCS:=${addprefix src/,os/geometry.d os/graphics.d os/sound.d os/system.d os/utils.d game/*.d}
GENERIC_OBJS:=${GENERIC_SRCS:.d=.o}
BARE_OBJS:=${addprefix bios-build/,${GENERIC_OBJS} os/drivers-bios.o}
TEST_SRCS:=${GENERIC_SRCS} src/os/drivers-test.d
ASSETS:=graphics/* graphics/*/* sound/*
ASM_COMMON:=${addprefix src/,common.asm common_defs.asm}

D_SDL2_LIBS=${addprefix -L,${shell sdl2-config --libs}}

src/os/drivers.di: src/os/drivers-bios.d
	${DMD} -o- -c -m32 ${DFLAGS} -Hfsrc/os/drivers.di src/os/drivers-bios.d


# BIOS boot

bios-build/payload.o: src/os/drivers.di ${GENERIC_SRCS} src/os/drivers-bios.d ${ASSETS}
	${DMD} ${BETTERC_DFLAGS} -odbios-build -version=bios -of$@ ${GENERIC_SRCS} src/os/drivers-bios.d

bios-build/boot.o: src/bios.asm ${ASM_COMMON} bios.ld
	mkdir -p bios-build
	nasm -f elf32 -i src/ -o bios-build/boot.o src/bios.asm

bios-build/image.bin: bios-build/payload.o bios-build/boot.o bios.ld
	ld -T bios.ld -o bios-build/image.bin --gc-sections -Map=bios-build/image.map -m elf_i386 bios-build/payload.o bios-build/boot.o


# Multiboot

mb-build/payload.o: src/os/drivers.di ${GENERIC_SRCS} src/os/drivers-bios.d ${ASSETS}
	${DMD} ${BETTERC_DFLAGS} -odmb-build -version=multiboot -of$@ ${GENERIC_SRCS} src/os/drivers-bios.d

mb-build/multiboot.o: src/multiboot.asm ${ASM_COMMON}
	mkdir -p mb-build
	nasm -f elf32 -i src/ -o mb-build/multiboot.o src/multiboot.asm

mb-build/xanthe: mb-build/multiboot.o mb-build/payload.o multiboot.ld
	ld -n -T multiboot.ld -o mb-build/xanthe --gc-sections -Map=mb-build/xanthe.map -m elf_i386 mb-build/multiboot.o mb-build/payload.o


# SDL

test-build/game: ${TEST_SRCS} src/os/drivers.di sdl2/*.d ${ASSETS}
	${DMD} -g -odtest-build -oftest-build/game -version=test -version=sdl -debug -unittest ${DFLAGS} ${D_SDL2_LIBS} ${TEST_SRCS}


# Targets

xanthe.iso: mb-build/xanthe
	mkdir -p iso-build/boot/grub
	cp grub.cfg iso-build/boot/grub
	cp mb-build/xanthe iso-build/boot
	grub-mkrescue -o xanthe.iso iso-build/

bios: bios-build/image.bin

mb: mb-build/xanthe

sdl: test-build/game

test: sdl bios mb
	./test-build/game --test

play: sdl
	./test-build/game

qemu-bios: bios
	${QEMU} ${QEMU_FLAGS} -drive file=bios-build/image.bin,format=raw

qemu-mb: mb
	${QEMU} ${QEMU_FLAGS} -kernel mb-build/xanthe

clean:
	rm -f src/os/drivers.di
	rm -rf bios-build
	rm -rf iso-build
	rm -rf mb-build
	rm -rf test-build

.PHONY: bios clean mb play qemu sdl test
